#!/usr/bin/env python3

"""
Author: Amanda van de Ligt

Script to: cluster an existing MMSeqs database at an input e value
Usage: python3 clusterMMSeqs.py <input_database> 
					<desired_min-seq-id> <output_filename>

"""
#Import Statements
from sys import argv
import subprocess
import os

def run_cluster(database_input, E_value, output_name):
    """runs mmseqs cluster in the cmd line to cluster input sequences

    Key inputs:
    database_input = str name of the mmseqs db to cluster
    E-value = E-value with which to cluster
    output_name = str name of the output prefix


    Outputs:
    None
    """
    #set clustered db name
    clustered_name = output_name + '_clustereddb'
    temp_dir = 'temp' + output_name + E_value

    #check if file exists before rerunning
    if not os.path.exists(output_name):
        cmd = 'mmseqs cluster {} {} temp_dir --min-seq-id {} --remove-tmp-files'.\
            format(database_input, clustered_name, E_value)
        print('running {}'.format(cmd))
        e = subprocess.check_output(cmd, shell=True)

        #if os.path.exists(clustered_name):
            #clustered_output = clustered_name
        #else:
           #raise Exception('no clustered output detected')

    else:
        clustered_output = output_name

    return None


def run_result2repseq(database_input, output_name):
    """runs mmseqs result2repseq in the cmd line to pull the repseqs

   	Key inputs:
    database_input = str name of the mmseqs db to cluster
    output_name = str name of the output prefix

    Outputs:
    None
    """
    #set clustered db name
    clustered_name = output_name + '_clustereddb'
    repseq_name = output_name + '_repseqs'


    cmd = 'mmseqs result2repseq {} {} {} --threads 6'.\
        format(database_input, clustered_name, repseq_name)
    print('running {}'.format(cmd))
    e = subprocess.check_output(cmd, shell=True)

    #if os.path.exists(repseq_name):
        #repseq_output = repseq_name
    #else:
        #raise Exception('no repseq output detected')


    return None


def run_convert2fasta(E_value, output_name):
    """runs mmseqs convert2fasta in the cmd line to make .fasta format output

   	Key inputs:
    database_input = str name of the mmseqs db to cluster
    output_name = str name of the output prefix

    Outputs:
    fasta_output = str name of output file from running clustering
    """
    #set clustered db name
    repseq_name = output_name + '_repseqs'
    fasta_name = output_name + 'repseqs_{}.fasta'.\
    			format(str(E_value))   


    cmd = 'mmseqs convert2fasta {} {}'.\
        format(repseq_name, fasta_name)
    print('running {}'.format(cmd))
    e = subprocess.check_output(cmd, shell=True)

    if os.path.exists(fasta_name):
        fasta_output = open(fasta_name)
    else:
        raise Exception('no .fasta output detected')


    return fasta_output


def run_mmseqs(database_input, E_value, output_name):
    """runs mmseqs in the cmd line to cluster input and return repseqs

    Key inputs:
    database_input = str name of the mmseqs db to cluster
    E-value = E-value with which to cluster
    output_name = str name of the output prefix


    Outputs:
    clustered_output = str name of output mmseqs db from running blastp
	"""
    run_cluster(database_input, E_value, output_name)
    run_result2repseq(database_input, output_name)
    run_convert2fasta(E_value, output_name)

    return 'Done'


if __name__ == "__main__":
	#print('inputs',str(argv[1]),str(argv[2]),str(argv[3]))
	run_mmseqs(argv[1],argv[2],argv[3])
