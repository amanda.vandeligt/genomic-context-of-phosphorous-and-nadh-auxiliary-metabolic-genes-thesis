#!/usr/bin/env python3

"""
Author: Amanda van de Ligt

Script to: take BLASTp tabulated output 6 to MAFFT input
Usage: python3 blast2mafft.py <blast_output.tab> <str_path_to_assemblies> 
    <str_path_to_output_storage>
"""

# Import statements
from sys import argv
import os.path

# Function definitions

#read file, pull SeqID, start pos, end pos
def parse_blast_output(filename):
    """parses blast tabular output for ID, start and stop position

    filename = name of file containing blast tabular output
    output:
    collected_info = list of tuples in order (subject accession, 
        start position, stop position)
    qry_acc = string of the query accession
    """
    file = open(str(filename), "r")
    file_contents = []
    for line in file:
        if not line.startswith("#"):
            file_contents.append(line)
    file.close()

    qry_acc = None
    collected_info = []
    for line in file_contents:
        split_line = line.split('\t')
        if qry_acc is None:
            qry_acc = split_line[0]
        sbj_acc = split_line[1]
        start_p = split_line[6]
        end_pos = split_line[7]
        collected_info.append((sbj_acc,start_p,end_pos))

    return collected_info, qry_acc


#connect protein accession to assembly file
def find_matching_assembly(assemblies_location,subject_accession):
    """locates assembly file contains a protein accession

    assemblies_location = str of path to folder containing mutliple assemblies
    subject_accession = str of a protien accession
    output is a string of the containing assembly's name
    """
    for file in os.listdir(assemblies_location):
        file_path = os.path.join(assemblies_location,file)
        check_file = os.path.isfile(file_path)
        #print(file_path,check_file)

        if file_path.endswith('.faa') or file_path.endswith('.fasta'):
            #print(file,os.path.isfile(file_path),subject_accession)

            with open(file_path) as open_file:
                #print(open_file)
                for line in open_file:
                    if subject_accession in line:
                        open_file.close()
                        return str(file_path)
    raise ValueError("containing assembly not found")


def pull_fasta_entry(assembly_file,subject_accession):
    """reads the relevant fasta entry for a protein accession in an assembly

    assembly_file = str filename of the assembly file
    subject_accession = str of a protein accession
    output is a string of the fasta entry
    """
    with open(assembly_file) as open_file:
        protein_flag = False
        for line in open_file:
            if subject_accession in line:
                protein_flag = True
                subject_header = line
                subject_sequence = ''
                continue
            if protein_flag:
                if line.startswith('>'):
                    protein_flag = False
                    break
                subject_sequence += line
    #subject_entry = subject_header + '\n' + subject_sequence
    return subject_header, subject_sequence


def trim_fasta_entry(fasta_header,fasta_sequence,start_pos,stop_pos):
    """trims a fasta sequence to only what's between a start and stop position

    fasta_header = str of a fasta header
    fasta_sequence = str of a protein sequence
    start_pos = float to begin the new sequence at
    stop_pos = float to end the new seqeunce at
    output is a string of the new trimmed fasta entry
    """
    fasta_sequence = fasta_sequence.replace('\n','')
    trimmed_sequence = fasta_sequence[int(start_pos):int(stop_pos)]
    trimmed_sequence_entry = fasta_header + trimmed_sequence
    return trimmed_sequence_entry


#construct fasta format fo protein sequences from start pos to end pos for each SeqID/AccID
def write_fasta_file(trimmed_fasta,accession_info,output_folder):
    """writes a fasta sequence to a file using the accession info as filename

    trimmed_fasta = str of a fasta entry
    accession_info = str containing the accession id of the fasta sequence
    output is None, produces a written file
    """
    if os.path.exists(output_folder):
        trimmed_fasta.strip()
        fasta_name = str(accession_info) + '_blasttrimmed.fasta'
        output_path = os.path.join(output_folder,fasta_name)
        fasta = open(output_path, 'w')
        fasta.write(trimmed_fasta)
        fasta.close()
        return None
    else:
        raise ValueError("output folder does not exist")


def main():
    """main function call"""
    hits_list,query = parse_blast_output(argv[1])
    #print(query,hits_list)
    created_multi_fasta = ''

    #create trimmed fastas for subjects
    for match in hits_list:
        #print('\n',match)
        #print(argv[2])
        #print('before containing file')
        containing_file = find_matching_assembly(argv[2],match[0])
        #print(containing_file)
        relevant_header, relevant_sequence = pull_fasta_entry(
            containing_file,match[0])
        #print(relevant_header,relevant_sequence)
        trimmed_match = trim_fasta_entry(
            relevant_header,relevant_sequence.strip(),match[1],match[2])
        #print(trimmed_match)
        write_fasta_file(trimmed_match,match[0],argv[3])
        created_multi_fasta += relevant_header + relevant_sequence + '\n'

    #create solo fasta for query
    #print('solo fasta')
    containing_file = find_matching_assembly(argv[2],query)
    relevant_header, relevant_sequence = pull_fasta_entry(
        containing_file,query)
    trimmed_match = trim_fasta_entry(
        relevant_header,relevant_sequence,0,(-1))
    write_fasta_file(trimmed_match,query,argv[3])
    created_multi_fasta += relevant_header + relevant_sequence
    write_fasta_file(created_multi_fasta,'total_blast_matches',argv[3])



    return None


if __name__ == "__main__":
    main()
    

