#!/usr/bin/env python3

"""
Author: Amanda van de Ligt

Script to: make AAI databases for all phages in a genome folder
Usage: python3 createAAIdbs_phagesinfolder.py <phage_proteome_folder> 
    <database_output_folder>

"""

# Import statements
from sys import argv
import subprocess
import os

# Function definitions
#pull .fasta files 
def pull_fasta_files(containing_folder):
    """identify .fasta files in a folder and pass their names to future functions

    containing_folder: str path from the working directory into where the .fasta
        files are being stored
    output is a list of str .fasta filenames to create databases from
    """
    fasta_files = []
    for filename in os.listdir(containing_folder):
        if '.fasta' in filename and 'genomedb' not in filename:
            #replaced_filename = filename.replace('.','_')
            fasta_files.append(filename)
    return fasta_files

#run java command one file at a time
def extract_AAI_database(fasta_name,containing_folder,database_folder):
    """run the EzAAI cmd to create a prot database in the cmdline

    Inputs:
    fasta_name = str name of a .fasta file
    containing_folder = str of the path from the working directory to the fasta 
        file contianing folder
    database_folder = str of the path from the working directory to the folder
        that will contain the created database

    None output is created
    """
    fasta_originfile = os.path.join(containing_folder,fasta_name)
    fasta_endingfile = os.path.join(database_folder,fasta_name.replace(
                                '.fasta','.db'))
    
    #java -jar EzAAI.jar convert -i <IN> -s <SEQ> -o <OUT>
    cmd = 'java -jar EzAAI_latest.jar extract -i {} -o {}'.\
        format(fasta_originfile,fasta_endingfile)
    e = subprocess.check_output(cmd, shell=True)
    #print(e)
    
    return e, fasta_originfile, fasta_endingfile

def main():
    """main function call"""
    pull_from = argv[1]
    send_to = argv[2]
    print("Using .fastas in:",pull_from,"\tSending .dbs to:",send_to)
    files_for_cmd = pull_fasta_files(pull_from)
    #print("Found .fasta files", files_for_cmd)
    for file in files_for_cmd:
        null, origin, end = extract_AAI_database(file,pull_from,send_to)
        #print(file,"origin & end:",origin,end, "e:", null)
    print("Finished with AAI database extraction")
    return None


if __name__ == "__main__":
    main()
    

