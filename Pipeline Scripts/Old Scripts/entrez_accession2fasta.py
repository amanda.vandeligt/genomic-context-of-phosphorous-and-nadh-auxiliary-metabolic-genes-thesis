#!/usr/bin/env python3

"""
Author: Amanda van de Ligt

Script to: pull fasta sequences from ncbi from given accessions
Usage: python3 entrez_accession2fasta.py

"""

#info: https://www.tutorialspoint.com/biopython/biopython_entrez_database.htm
# https://stackoverflow.com/questions/43049794/efetch-with-too-many-accession-number



#Import Statements
from sys import argv
import subprocess
import os.path
from biopython import Entrez


Entrez.email = 'myemail@ncbi.nlm.nih.gov' # provide  email address
db = 'protein'                              # set search to database
#paramEutils = { 'usehistory':'Y' }# Use Entrez search history to cache results


def testing_command():
    Entrez.tool = 'Demoscript'
    info = Entrez.einfo()
    data = info.read()
    record = Entrez.read(info)
    #record is a dictionary with one key: the database list


def fetch_fastas(id_list):
    """fetches the fasta sequences of all acession ids in a list

    Key inputs:
    id_list = list of str accession ids

    Outputs:
    fasta_sequences = list of str of fasta sequences
    """
    pulled_accessions = Entrez.efetch(db = "protein",rettype = "fasta", 
                          id=id_list)#'csv list ids'.join(accessions))
    #record = SeqIO.read( pulled_accessions, "fasta" ) #probs not needed
    return pulled_accessions


def open_read_list(filename):
    """transforms a newline list into a csv list

    Key inputs:
    filename = name of file containing ncbi accessions seperated by '\n'

    Outputs:
    accessions = list of str format accessions
    """
    file = open(str(filename), "r")
    accessions = []
    for line in file:
        line.strip()
        accessions.append(line)
    file.close()
    return accessions


if __name__ == "__main__":
    list_of_accessions = open_read_list(argv[1])
    print(list_of_accessions)