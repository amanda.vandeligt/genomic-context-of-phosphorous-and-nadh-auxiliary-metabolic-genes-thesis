#!/usr/bin/env python3

"""
Author: Amanda van de Ligt

Script to: make AAI databases for all phages in a folder
Usage: python3 createAAIdbs_phagesinfolder.py <phage_proteome_folder> 
	<database_output_folder>

"""

# Import statements
from sys import argv
import subprocess
import os

# Function definitions
#pull .faa files 
def pull_faa_files(containing_folder):
	"""identify .faa files in a folder and pass their names to future functions

	containing_folder: str path from the working directory into where the .faa
		files are being stored
	output is a list of str .faa filenames to create databases from
	"""
	faa_files = []
	for filename in os.listdir(containing_folder):
		if '.faa' in filename and 'proteome' not in filename:
			replaced_filename = filename.replace('.','_')
			faa_files.append(replaced_filename)
	return faa_files

#run java command one file at a time
def create_AAI_database(faa_name,containing_folder,database_folder):
	"""run the EzAAI cmd to create a prot database in the cmdline

	Inputs:
	faa_name = str name of a .faa file
	containing_folder = str of the path from the working directory to the faa 
		file contianing folder
	database_folder = str of the path from the working directory to the folder
		that will contain the created database

	None output is created
	"""
	faa_originfile = os.path.join(containing_folder,faa_name)
	faa_endingfile = os.path.join(database_folder,faa_name)
	
	#java -jar EzAAI.jar convert -i <IN> -s <SEQ> -o <OUT>
	cmd = 'java -jar EzAAI.jar convert -i {} -s prot -o {}'.\
		format(faa_originfile,faa_endingfile)
    e = subprocess.check_output(cmd, shell=True)
	
	return None

def main():
    """main function call"""
    pull_from = argv[1]
    send_to = argv[2]
    files_for_cmd = pull_faa_files(pull_from)
    for file in files_for_cmd:
    	null = create_AAI_database
    return None


if __name__ == "__main__":
    main()
    

