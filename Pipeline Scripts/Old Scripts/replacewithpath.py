#!/usr/bin/env python3

"""
Author: Amanda van de Ligt

Script to: split a fasta file with multiple sequences into seperate
    fasta files for each sequence and retain original file
"""

# Import statements
from sys import argv

# Function definitions
#read in fasta file
def readfile(filename):
    """create string containing contents of a file

    filename = name of file to be read
    output is a string 
    """
    file = open(str(filename), 'r')
    output = file.read()
    #output = output.split('\n')
    file.close()
    return output


def replace_filename(file,folder_name):
    """create a newline delmited file of filepaths to filenames

    file = list of filenames
    output is a single string 
    """
    splitfile = file.split('\n')
    print(splitfile)
    replaced_output = ''
    for line in splitfile:
        print(line)
        backslashstr = "\\"
        backslashstr.strip()
        replaced_line = backslashstr + str(folder_name) + backslashstr + "'" + str(line) + "'.fasta\n"
        replaced_output += replaced_line
        #print(replaced_line)
    replaced_output.strip()
    newfile = open('pathednames.txt', 'w')
    newfile.write(replaced_output)
    newfile.close()
    return replaced_output

if __name__ == "__main__":

    # implement main code here
    names = readfile(argv[1])
    replace_filename(names,'PF02562phagegenomes')