#!/usr/bin/env python3

"""
Author: Amanda van de Ligt

Script to: rename .faa assemblies downloaded from entrez
    to contain accession and organism name
Usage: python3 rename2organism.py <folder_containing_assemblies>

"""

# Import statements
from sys import argv
import os
import shutil

# Function definitions
def access_folder(folder):
    """take name of a folder and iterate over contents to rename assemblies

    folder = a folder containing .faa proteome assemblies
    
    """
    #do i need to pull the pathname here? i think i did
    #cwd = os.getcwd()
    #print(cwd)
    folder_path = os.path.realpath(folder)
    #print(folder_path)
    for item in os.listdir(folder_path):
        item_path = os.path.join(folder_path,item)
        if item_path.endswith('.faa'):
            #print('in folder')
            #print('item path: ',item_path)
            #item_path is a file to be renamed
            organism_name = find_organism_name(item_path)
            #pull the organism name
            if not item_path.endswith('protdb.faa'):
                rename_file(item_path, organism_name)
            #rename the file
            continue
        #continue to next item in  folder
        else:
            continue
    return 'files renamed with organism name'


def find_organism_name(proteome_file):
    """find the organism name inside a proteome file

    proteome_file = real path to a proteome assembly file
    output is str of organism name
    """
    with open(proteome_file) as open_file:
        for line in open_file:
            if line.startswith('>'):
                first_line = line.strip()
                open_bracket = first_line.find('[')
                close_bracket = first_line.find(']')
                organsim = first_line[(open_bracket+1):(close_bracket)]
                break
            else:
                continue
    return organsim


def rename_file(path_to_assembly, organsim_str):
    """rename a .faa assembly file to include organism name

    path_to_assembly = str real path to an assembly file
    organism_str = str of the organism for the given assembly
    No output
    """
    organsim_str = organsim_str.replace(' ','_')
    organsim_str = organsim_str.replace('.',',')
    organsim_str = organsim_str.replace('/','-')
    ext_index1 = path_to_assembly.find('ViralProj')
    ext_index2 = path_to_assembly.find('ASM')
    if ext_index1 == -1:
        ext_index = ext_index2
    else:
        ext_index = ext_index1
    new_name = path_to_assembly[0:(ext_index)] + '|_' + organsim_str + '.faa'
    os.rename(path_to_assembly,new_name)



if __name__ == "__main__":
    # implement main code here
    #folder_name = argv[1]
    #print(folder_name)
    end_text = access_folder(argv[1])
    print(end_text)
