#!/usr/bin/env python3

"""
Author: Amanda van de Ligt

Script to: make AAI databases for all extracted genomes in a folder
Usage: python3 createAAIdbs_phagesinfolder.py <phage_proteome_folder> 
    <database_output_folder>

"""

# Import statements
from sys import argv
import subprocess
import os

# Function definitions
#pull .fastadb files 
def pull_fastadb_files(containing_folder):
    """identify .fastadb files in a folder and pass their names to future functions

    containing_folder: str path from the working directory into where the .fastadb
        files are being stored
    output is a list of str .fastadb filenames to create databases from
    """
    fastadb_files = []
    for filename in os.listdir(containing_folder):
        if '.fasta.db' in filename:
            #replaced_filename = filename.replace('.','_')
            fastadb_files.append(filename)
    return fastadb_files

#run java command one file at a time
def create_AAI_database(fastadb_name,containing_folder,database_folder):
    """run the EzAAI cmd to create a prot database in the cmdline

    Inputs:
    fastadb_name = str name of a .fastadb file
    containing_folder = str of the path from the working directory to the fastadb 
        file contianing folder
    database_folder = str of the path from the working directory to the folder
        that will contain the created database

    None output is created
    """
    fastadb_originfile = os.path.join(containing_folder,fastadb_name)
    fastadb_endingfile = os.path.join(database_folder,fastadb_name.replace('.fasta.db','.db'))
    
    #java -jar EzAAI.jar convert -i <IN> -s <SEQ> -o <OUT>
    cmd = 'java -jar EzAAI_latest.jar convert -i {} -s nucl -o {}'.\
        format(fastadb_originfile,fastadb_endingfile)
    e = subprocess.check_output(cmd, shell=True)
    #print(e)
    
    return e, fastadb_originfile, fastadb_endingfile

def main():
    """main function call"""
    pull_from = argv[1]
    send_to = argv[2]
    print("Using .fastadbs in:",pull_from,"\tSending .dbs to:",send_to)
    files_for_cmd = pull_fastadb_files(pull_from)
    #print("Found .fastadb files", files_for_cmd)
    for file in files_for_cmd:
        null, origin, end = create_AAI_database(file,pull_from,send_to)
        #print(file,"origin & end:",origin,end, "e:", null)
    print("Finished with AAI database creation")
    return None


if __name__ == "__main__":
    main()
    

