#!/usr/bin/env python3

"""
Author: Amanda van de Ligt

Script to: convert Hit table .csv download from BLAST results into list
    of genome accession numbers in newline delimited string
"""

# Import statements
from sys import argv

# Function definitions
#read in fasta file
def readfile(filename):
    """create string containing contents of a file and repalce '/' with '_'

    filename = name of file to be read
    output is a single string 
    """
    file = open(str(filename), 'r')
    output = file.read()
    file.close()
    return output

def keep_second_column(raw_accession_entry):
    """remove everything that isn't column 2 of a csv file 

    raw_accession_entry = one line of the csv file as a str
    output is a string of the accession without trailing info
    """
    part_accession = raw_accession_entry.split(',')
    #print(part_accession[1])
    #print(type(part_accession))
    column_two = part_accession[1]
    final_part_accession = column_two.split('.')
    return final_part_accession[0]

def edit_accession_list(accession_list):
    """remove all accession trailing from the accession list

    accession_list = list from column 1 of the hit table .csv
                    newline seperated
    output is new list of the accessions in same order with trailing
        of all entries removed
    """
    new_accession_str = ''
    accession_list = accession_list.strip()
    accessions_as_list = accession_list.split('\n')
    for accession in accessions_as_list:
        #print(accession)
        new_accession = keep_second_column(accession)
        if new_accession in new_accession_str:
            continue
        else:
            new_accession_str += new_accession + '\n'
    new_accession_str.strip()
    return new_accession_str


if __name__ == "__main__":

    # implement main code here
    file = readfile(argv[1])
    all_accessions = edit_accession_list(file)
    print(all_accessions)

