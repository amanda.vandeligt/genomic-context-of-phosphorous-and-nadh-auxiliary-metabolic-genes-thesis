#!/usr/bin/env python3

"""
Author: Amanda van de Ligt

Script to: convert .fasta file from MMSeqs results into list
    of genome accession numbers in newline delimited string

Usage: python3 createMMSeqsaccessionlist.py <input_fasta>
"""

# Import statements
from sys import argv

# Function definitions
def find_single_record(read_file):
    """Retreive a single record from fasta file

    read_file = contents of a file in string format
    Output is a single record from the input file
    """
    current_record = []
    for line in read_file:
        if not line.strip():
            continue
        if line.startswith('>'):
            yield current_record
            current_record = [line.strip()]
        else:
            #current_record.append(line.strip())
            continue
    if current_record:
        yield current_record

def remove_after(raw_accession_entry):
    """remove anything from accession col trailing the accession 

    raw_accession_entry = one line of the csv file
    output is a string of the accession without trailing info
    """
    #print('input remove_after', raw_accession_entry)
    part_accession = raw_accession_entry.split('.1')
    #print(part_accession)
    return part_accession[0]

def edit_accession_list(accession_list):
    """remove all sequence trailing from the accession list

    accession_list = list from column 1 of the hit table .csv
                    newline seperated
    output is new list of the accessions in same order with trailing
        of all entries removed
    """
    new_accession_str = ''
    accessions_as_list = str(accession_list).split('\n')
    for accession in accessions_as_list:
        accession = str(accession)
        #print('string',accession)
        new_accession = remove_after(accession)
        if new_accession in new_accession_str:
            continue
        else:
            new_accession_str += new_accession[1:] + '.1'
    new_accession_str.strip()
    return new_accession_str


if __name__ == "__main__":

    # implement main code here
    record_info_collection = []
    with open(argv[1]) as open_file:
        for single_record in find_single_record(open_file):
            #print('record',single_record)
            if len(single_record) > 0:
                single_record = single_record[0]
            single_accession = edit_accession_list(single_record)
            #print('accession?', single_accession)
            #single_accession = single_accession[1:]
            record_info_collection.append(single_accession)
    output_file = open('{}_accessions.txt'.format(str(argv[1])), 'w')
    output_file.write('\n'.join(record_info_collection[1:]))
    output_file.close()
