#!/usr/bin/env python3

"""
Author: Amanda van de Ligt

Script to: rename .faa assemblies inside a Newwick file
    to contain accession and organism name
Usage: python3 newwick_rename.py <folder_containing_faas> <newwick_file>

"""

# Import statements
from sys import argv
import os
import shutil

# Function definitions
def build_dictionary(folder):
    """take name of a folder and iterate over contents to build a dictionary

    folder = a folder containing .faa proteome assemblies
    
    """
    #do i need to pull the pathname here? i think i did
    #cwd = os.getcwd()
    #print(cwd)
    dictionary_old_new = {}
    folder_path = os.path.realpath(folder)
    #print(folder_path)
    for item in os.listdir(folder_path):
        item_path = os.path.join(folder_path,item)
        if item_path.endswith('.faa'):
            #print('in folder')
            #print('item path: ',item_path)
            path_to,old_name = os.path.split(item)
            #item_path is a file to be renamed
            new_name = find_new_name(item_path)
            #pull the organism name
            dictionary_old_new[str(old_name)] = str(new_name)
            continue
        #continue to next item in  folder
        else:
            continue
    return dictionary_old_new


def find_new_name(proteome_file):
    """find the organism name inside a proteome file

    proteome_file = real path to a proteome assembly file
    output is str of organism name
    """
    with open(proteome_file) as open_file:
        for line in open_file:
            if line.startswith('>'):
                first_line = line.strip()
                open_bracket = first_line.find('[')
                close_bracket = first_line.find(']')
                organsim = first_line[(open_bracket+1):(close_bracket)]
                break
            else:
                continue
    path_to,old_name = os.path.split(proteome_file)
    organsim_str = organsim.replace(' ','_')
    organsim_str = organsim_str.replace('.',',')
    organsim_str = organsim_str.replace('/','-')
    ext_index1 = old_name.find('ViralProj')
    ext_index2 = old_name.find('ASM')
    if ext_index1 == -1:
        ext_index = ext_index2
    else:
        ext_index = ext_index1
    new_name = old_name[0:(ext_index)] + '|_' + organsim_str
    return new_name


def replace_in_file(text_file, dictionary_of_names):
    """replace the old names of .faa files in a text file with a new name 

    text_file = str  path to a text file
    dictionary_of_names = dictionary of assembly name:new assembly name
    No output
    """
    textfile_path = os.path.realpath(text_file)
    with open(textfile_path) as open_file:
        file_contents = open_file.read()
        for key,value in dictionary_of_names.items():
            #print(key, '->', value)
            file_contents = file_contents.replace(str(key),str(value))
        new_file_name = text_file.strip(".") + "_replaced"
        with open(new_file_name, "w") as newfile:
            newfile.write(file_contents)
    return 'new file with replaced names has been created'

    



if __name__ == "__main__":
    # implement main code here
    #folder_name = argv[1]
    #print(folder_name)
    name_dictionary = build_dictionary(argv[1])
    end_text = replace_in_file(argv[2],name_dictionary)
    print(end_text)
