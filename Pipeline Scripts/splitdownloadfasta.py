#!/usr/bin/env python3

"""
Author: Amanda van de Ligt

Script to: split a fasta file with multiple sequences into seperate
    fasta files for each sequence and retain original file
"""

# Import statements
from sys import argv

# Function definitions
#read in fasta file
def readfile(filename):
    """create string containing contents of a file and repalce '/' with '_'

    filename = name of file to be read
    output is a single string 
    """
    file = open(str(filename), 'r+')
    output = file.read()
    replaced_output = output.replace('/','_')
    file.write(replaced_output)
    file.close()
    return replaced_output

#split fasta into seperate sequences
def fastasplit(filename):
    """converts a fasta format file into a list of nucleotide sequences

    filename = txt file in fasta format
    output is a list of lists formatted [header_line, nucleotide_sequence]
    """
    list_entries = filename.split('>')
    list_entries = list_entries[1:]
    return list_entries

#write new fasta files for each individual sequence
#will have same file name as the fasta header
def fastawrite(sequence_list):
    """writes a list of sequences into seperate fasta files

    sequence_list = list of sequences preceeded by a line of header
    output is multiple written files
    """
    for sequence in sequence_list:
        sequence = sequence.strip()
        splitsequence = sequence.split('\n')
        sequence_header = splitsequence[0:1]
        #sequence_body = splitsequence[1:]
        #print(sequence_header[0])
        newfilename = str(sequence_header[0]) + '.fasta'
        #print(f'writing {newfilename}')
        newfile = open(newfilename, 'w')
        newfile.write('>' + sequence)
        newfile.close()





if __name__ == "__main__":

    # implement main code here
    file = readfile(argv[1])
    allsequences = fastasplit(file)
    fastawrite(allsequences)