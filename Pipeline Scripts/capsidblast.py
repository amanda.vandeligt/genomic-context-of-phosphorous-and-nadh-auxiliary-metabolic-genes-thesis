#!/usr/bin/env python3

"""
Author: Amanda van de Ligt

Script to: BLAST search a capsid protein against viral assemblies
Usage: python3 capsidblast.py <folder_containing_assemblies> <capsid_sequence>
    <desired_E-value> <name_for_database> <output_filename>
        *----Use complete filepaths----*

"""

#Import Statements
from sys import argv
import subprocess
import os.path

def concatenate_to_one_file(folder_of_assemblies, file_type, dbtype):
    """concatenate all assemblies in one folder into one multifasta file

    Key inputs:
    folder_to_assemblies = str path to folder containing desired assemblies
    file_type = str of the extension on files to be concatenated
    dbtype = str denoting what type of sequences are contained in the files

    Outputs:
    created_catenation = multifasta file containing all assemblies from folder
    """
    #print('assemblies location:',folder_of_assemblies)
    database_name = '{}db{}'.format(dbtype,file_type)
    database_path = os.path.join(folder_of_assemblies,database_name)

    if os.path.isfile(database_path):
        return database_path

    elif os.path.isdir(folder_of_assemblies):
        
        cmd = 'cat {}/*{} > {}'.\
                format(folder_of_assemblies, file_type, database_path)
        print('running {}'.format(cmd))
        e = subprocess.check_output(cmd, shell=True)

        #print('database path:',database_path,'\ne check',e)
        if os.path.isfile(database_path):
            return database_path
        else:
            raise Exception('concatenation to one file failed')

    else:
        raise Exception('path to folder_of_assemblies is invalid')

    return database_path

def make_blast_database(file_for_database, file_type, dbtype, database_name):
    """run cmd makeblast db in the command line on a given multifasta file

    Key inputs:
    file_for_database = str filepath to a multifasta to make into the blast db
    file_type = str of the extension on files to be concatenated
    dbtype = str denoting what type of sequences are contained in the files
    database_name = str of what to call the database

    Outputs:
    blast_db = filepath to the blastdb
    """
    folder_path, filename_for_database = os.path.split(file_for_database)
    database_filepath = os.path.join(folder_path,database_name)

    cmd = 'makeblastdb -in {} -parse_seqids -dbtype {} -out {} -title {}'.\
            format(file_for_database,dbtype,database_filepath,database_name)
    print('running {}'.format(cmd))
    e = subprocess.check_output(cmd, shell=True)

    #print('database path:',database_filepath,'\ne check',e)
    if os.path.isfile('{}.phr'.format(database_filepath)):
        return database_filepath
    else:
        raise Exception('database construction failed')

    return database_filepath

def search_blastp_database(database_name, capsid_sequence, output_name, evalue, thread_num):
    """run cmd blastp to search for capsid sequence in a given created database

    Key inputs:
    database = str filepath to a blast db
    capsid_sequence = str of the path to a fasta of the capsid sequence
    output_name = str of what to name the blast results file
    evalue = int of what evalue to use
    thread_num = int of how many computer threads to use

    Outputs:
    blast_db = filepath to the blastdb
    """
    folder_path, filename_for_capsid = os.path.split(capsid_sequence)
    output_filepath = os.path.join(folder_path,output_name)
    
    #print('blast db name:',database_name)
    #print('\n output filepath:',output_filepath)

    cmd = 'blastp -query {} -db {} -out {} -outfmt 6 -evalue {} -num_threads {}'.\
            format(capsid_sequence, database_name, output_filepath, 
                    evalue, thread_num)
    print('running {}'.format(cmd))
    e = subprocess.check_output(cmd, shell=True)

    if os.path.isfile(output_filepath):
        return output_filepath
    else:
        raise Exception('blastps search failed')

    return output_filepath


if __name__ == "__main__":
    #print('inputs',str(argv[1]),str(argv[2]),str(argv[3]),str(argv[4]))
    file_type = '.faa' #what extension are the desired files saved as
                        #usually '.faa' or '.fasta', include the '.'
    sequence_type = 'prot' #type must equal 'prot' or 'nucl'
    number_threads = 2 #how many threads to use for blast search
    real_path_assemblies = os.path.realpath(argv[1])
    real_path_capsid = os.path.realpath(argv[2])

    conatenated_file = concatenate_to_one_file(
                            real_path_assemblies, file_type, sequence_type)
    database = make_blast_database(
                            conatenated_file, file_type, sequence_type, argv[4])
    output = search_blastp_database(
                            database, real_path_capsid, argv[5], argv[3], 
                            number_threads)
