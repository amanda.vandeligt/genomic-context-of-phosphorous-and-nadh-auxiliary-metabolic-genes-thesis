#!/usr/bin/env python3

"""
Author: Amanda van de Ligt

Script to: rename .faa files downloaded from entrez
    as contained in their downloaded folders
"""

# Import statements
from sys import argv
import os
import shutil

# Function definitions
def access_folders(base_folder):
    #take name of root folder and iterate over contents
    #do i need to pull the pathname here? i think i did
    #cwd = os.getcwd()
    #print(cwd)
    base_folder_path = os.path.realpath(base_folder)
    #print(base_folder_path)
    for item in os.listdir(base_folder_path):
        item_path = os.path.join(base_folder_path,item)
        if is_folder(item_path):
            #print('in folder')
            print('item path: ',item_path)
            #item_path now functions as the proteome folder
            #following two commented out lines don't work
            #proteome_folder_path = os.path.realpath(item)
            #print('protpath',proteome_folder_path)
            access_file(item_path,base_folder_path)
            #run rename and copy functions
            continue
        #continue to next item in base folder
        else:
            continue
    return 'success'



#check if item in folder is a folder
def is_folder(checked_item):
    if os.path.isdir(checked_item):
        return True
    else:
        return False


#find .faa file in folder
def access_file(proteome_folder, base_folder_path):
    #print('input to access_file: ', proteome_folder, base_folder_path)
    for file in os.listdir(proteome_folder):
        if is_faa(file):
            #print(file)
            #file now functons as the desired .faa file
            old_file_path = os.path.join(proteome_folder,file)
            #print('old file path: ',old_file_path)
            rename_and_copy(old_file_path, proteome_folder, base_folder_path)
            #copy to base folder
            #rename to proteome folder + .faa


#check if item in folder is named 'protein.faa'
def is_faa(file_name):
    if file_name.lower().endswith('.faa'):
        return True
    else:
        return False


#rename .faa file to the name of the folder containing it
    #input .faa file and folder name
#copy .faa file into root directory
def rename_and_copy(faa_file,proteome_folder_path,
base_folder_path):
    #print('input to rename_and_copy: ', faa_file,proteome_folder_path,base_folder_path)
    split_proteome_folder_head,split_proteome_folder_tail = os.path.split(proteome_folder_path)
    #print(faa_file)
    split_proteome_folder_tail = faa_file.replace('.','_')
    split_proteome_folder_tail = split_proteome_folder_tail + '.faa'
    #print(split_proteome_folder_tail)
    destination_file = os.path.join(base_folder_path,split_proteome_folder_tail)
    #print('input to copy: ',faa_file,destination_file)
    shutil.copy(faa_file,destination_file)
    





if __name__ == "__main__":
    # implement main code here
    base_folder_name = argv[1]
    #print(base_folder_name)
    end_text = access_folders(argv[1])
    print(end_text)
